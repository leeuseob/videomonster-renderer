function Exec(shell) {
  return new Promise((resolve, reject) => {
    const exec = require('child_process').exec
    exec(shell, (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`)
      }
      console.log('stdout ', stdout)
      console.log('stderr ', stderr)
      resolve()
    })
  })
}

function sleep(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms)
  })
}

async function func() {
  const fs = require(`fs`)

  function AccessAsync(path) {
    return new Promise((resolve, reject) => {
      fs.access(path, err => {
        if (err) resolve(false)
        else resolve(true)
      })
    })
  }

  function ReadFileAsync(path, options) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, options, (err, data) => {
        if (err) reject(err)
        else resolve(data)
      })
    })
  }

  function WriteFileAsync(path, data) {
    return new Promise((resolve, reject) => {
      fs.writeFile(path, data, err => {
        if (err) reject(err)
        else resolve()
      })
    })
  }

  const config = require(`./config`)
  const video = require(`./modules/video`)
  const global = require(`./global`)

  // 시작 전에 VM DEVICE에 생성된 TOKEN_ID 파일이 있는지 검사한다. 있으면 그대로 사용한다.
  let token = ``
  if (await AccessAsync(config.tokenPath)) {
    token = await ReadFileAsync(config.tokenPath, 'utf8')
    console.log(token)
  }
  // 없으면 TOKEN_ID를 새로 하나 생성하여 LOCAL에 저장한다.
  else {
    token = require(`guid`).create().value
    await WriteFileAsync(config.tokenPath, token)
  }

  console.log(`start!`)

  const socket = require(`socket.io-client`)(`http://10.0.0.7:3000`, {
    transports: [`websocket`],
    query: {
      token: token
    }
  })

  const {
    localPath
  } = config

  const ERenderStatus = {
    NONE: 0,
    VIDEO: 1,
    AUDIO: 2,
    MAKEMP4: 3
  }
  let renderStatus = 0

  // let isImageRendering = false
  let isTemplateConfirmRendering = false  // 현재 렌더러가 Template Confirm Rendering을 수행하는지 여부

  let isAudioRendering = false    // 오디오 렌더링 수행중?
  let isVideoRendering = false    // 비디오 렌더링 수행중?
  let isMerging = false           // 비디오 Merging 수행중?

  socket.on(`connect`, () => {
    console.log(`Connected!`)
    console.log(`videoclient`)
    socket.emit(`regist`, `videoclient`)
  })

  socket.on(`disconnect`, () => {
    console.log(`Disconnected!`)
  })

  // 렌더 서버에서 클라이언트가 네트워크 문제 등의 이유로 재접속 되었을 때, 작업을 수행중인지 물어본다.
  // 만약 작업을 수행하고 있지 않다면 (VM이 재부팅되거나, 프로세스가 다시 시작되었을 경우) 에러 코드를 서버에 전송한다.
  // Template Confirm Rendering 수행 여부 확인
  socket.on(`is_stopped_template_confirm_rendering`, async data => {
    const { currentGroupIndex } = data
    if (isTemplateConfirmRendering == false) {
      socket.emit(`template_confirm_render_completed`, {
        currentGroupIndex,
        errCode: `ERR_TEMPLATE_CONFIRM_RENDER_STOPPED`
      })
    }
  })

  // Audio Rendering 수행 여부 확인
  socket.on(`is_stopped_audio_rendering`, async data => {
    const { currentGroupIndex } = data
    if (isAudioRendering == false) {
      socket.emit(`audio_render_completed`, {
        currentGroupIndex,
        errCode: `ERR_AUDIO_RENDER_STOPPED`
      })
    }
  })
  
  // Video Rendering 수행 여부 확인
  socket.on(`is_stopped_video_rendering`, async data => {
    const { currentGroupIndex } = data
    if (isVideoRendering == false) {
      socket.emit(`video_render_completed`, {
        currentGroupIndex,
        errCode: `ERR_VIDEO_RENDER_STOPPED`
      })
    }
  })
  
  // Video Merging 수행 여부 확인
  socket.on(`is_stopped_merging`, async data => {
    const { currentGroupIndex } = data
    if (isMerging == false) {
      socket.emit(`merge_completed`, {
        currentGroupIndex,
        errCode: `ERR_MERGE_STOPPED`
      })
    }
  })

  // Template Confirm Render 시작
  socket.on(`template_confirm_render_start`, async (data) => {
    isTemplateConfirmRendering = true
    let {
      currentGroupIndex,

      aepPath,
      audioPath,
      videoPath,
      fontPath,
      
      frameRate,
      hashTagString,
      totalFrameCount
    } = data

    let startFrame = 0
    let endFrame = totalFrameCount - 1

    console.log(data)

    try {
      // AEP 파일이 존재하는지 검사한다. (10초 내로 찾지 못하면 에러 코드를 전송한다.)
      for (let i = 0; i < 10; i++) {
        console.log(`Check aep path...`)
        if (await AccessAsync(aepPath)) break
        await sleep(1000)
        if (i == 9) throw `ERR_NO_AEP_FILE`
      }
      
      // 폰트 설치
      await global.InstallFont(fontPath)

      // Rendered Frame Count 0으로 초기화 (렌더링 진행률 보고)
      video.ResetTotalRenderedFrameCount()

      // 오디오 렌더링
      await video.AudioRender(aepPath, audioPath, totalFrameCount)
      
      // 비디오 렌더링 (모든 프레임을 TIFF 파일로 전부 뽑아낸다.)
      renderStatus = ERenderStatus.VIDEO
      ReportProgress(currentGroupIndex, 0)
      const res = await video.VideoRender(0, aepPath, startFrame, endFrame, hashTagString)
      
      // 각 Frame별 렌더링 시간을 계산한다.
      const frameDuration = {}
      let totalTime = 0
      Object.keys(res).forEach(key => {
          const ms = res[key]
          const newKey = startFrame + Number(key) - 1

          frameDuration[newKey] = ms
          totalTime += ms
      })
      Object.keys(frameDuration).forEach(key => {
        frameDuration[key] /= totalTime
      })

      // 모든 TIFF 파일을 취합하여 h264로 인코딩한다.
      renderStatus = ERenderStatus.MAKEMP4
      await video.MakeMP4(0, videoPath, hashTagString, frameRate)
      
      // Merge를 수행한다. (Template Confirm Rendering은 렌더러를 1개만 사용하므로 Merge는 별로 의미가 없음.)
      await video.Merge(1, videoPath)
      // 비디오 파일에 Audio를 입힌다.
      await video.ConcatAudio(videoPath, audioPath)

      socket.emit(`template_confirm_render_completed`, {
        currentGroupIndex,
        frameDuration,
        errCode: null
      })
    }
    catch (e) {
      console.log(e)
      socket.emit(`template_confirm_render_completed`, {
        currentGroupIndex,
        frameDuration: null,
        errCode: e
      })
    }
    renderStatus = ERenderStatus.NONE
    isTemplateConfirmRendering = false
  })
  
  // 비디오 분산 렌더링 시작
  socket.on(`video_render_start`, async (data) => {
    isVideoRendering = true
    let {
      currentGroupIndex,
      rendererIndex,

      aepPath,
      videoPath,
      fontPath,

      startFrame,
      endFrame,
      frameRate,
      hashTagString
    } = data

    console.log(data)

    try {
      // AEP 파일이 존재하는지 검사한다. (10초 내로 찾지 못하면 에러 코드를 전송한다.)
      for (let i = 0; i < 10; i++) {
        console.log(`Check aep path...`)
        if (await AccessAsync(aepPath)) break
        await sleep(1000)
        if (i == 9) throw `ERR_NO_AEP_FILE`
      }
      
      // 폰트 설치
      await global.InstallFont(fontPath)

      // Rendered Frame Count 0으로 초기화 (렌더링 진행률 보고)
      video.ResetTotalRenderedFrameCount()

      // 비디오 렌더링 (프레임을 TIFF 파일로 전부 뽑아낸다.)
      // startFrame, endFrame까지 뽑아낸다.
      renderStatus = ERenderStatus.VIDEO
      ReportProgress(currentGroupIndex, rendererIndex)
      await video.VideoRender(rendererIndex, aepPath, startFrame, endFrame, hashTagString)
      
      // 렌더링한 TIFF 파일들을 취합하여 h264로 인코딩한다.
      renderStatus = ERenderStatus.MAKEMP4
      await video.MakeMP4(rendererIndex, videoPath, hashTagString, frameRate)

      socket.emit(`video_render_completed`, {
        currentGroupIndex,
        errCode: null
      })
    }
    catch (e) {
      console.log(e)
      socket.emit(`video_render_completed`, {
        currentGroupIndex,
        errCode: e
      })
    }
    renderStatus = ERenderStatus.NONE
    isVideoRendering = false
  })

  // 1초에 한번씩 렌더서버에 진행률을 보고한다.
  function ReportProgress(currentGroupIndex, rendererIndex) {
    if (renderStatus != ERenderStatus.NONE) {
      switch (renderStatus) {
        case ERenderStatus.VIDEO:
        case ERenderStatus.MAKEMP4:
          socket.emit(`report_progress`, {
            currentGroupIndex,
            renderStatus,
            renderedFrameCount: video.GetTotalRenderedFrameCount()
          })
          break
      }

      setTimeout(ReportProgress, 1000, currentGroupIndex, rendererIndex)
    }
  }

  // Merging 시작 (분산 렌더링된 영상 파일들을 하나로 합치는 작업)
  // 렌더러 그룹의 각 0번 렌더러가 단독으로 수행
  socket.on(`merge_start`, async (data) => {
    isMerging = true
    const {
      currentGroupIndex,
      rendererCount,
      videoPath,
      audioPath
    } = data
    console.log(data)

    try {
      // 분산 렌더링된 영상들을 하나로 합친다.
      await video.Merge(rendererCount, videoPath)

      // 영상에 오디오를 입힌다.
      await video.ConcatAudio(videoPath, audioPath)

      socket.emit(`merge_completed`, {
        currentGroupIndex,
        errCode: null
      })
    }
    catch (e) {
      console.log(e)
      socket.emit(`merge_completed`, {
        currentGroupIndex,
        errCode: e
      })
    }

    isMerging = false
  })
}

func()